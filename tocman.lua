blogman = blogman or {config={}}
blogman.checkpost = function(url)
	local y,m,d,pid	= url:match("(%d+)[/%-](%d+)[/%-](%d+)[/%-](.+)")
	if not y and not m and not d and not pid then
		return false, "MALFORMED URL"
	end
	if blogman.toc[y] then
		if config.dirres == "month" then
			if blogman.toc[y][m] then
-- MK1
				return blogman.toc[y][m][pid] or nil, "NO POSTS FOR MONTH "..m
			end
		elseif config.dirres == "day" then
			return blogman.toc[y][m][d][pid] or nil, table.concat({"NO POSTS FOR DATE: "..y,m,d},"-") -- TODO: week?
		else
			-- cycle through each entry in table looking for matches to ID. if their are any, check to make sure month and day match.
			return blogman.toc[y][pid] or nil, table.concat({"NO POSTS FOR DATE: "..y,m,d},"-") -- TODO: week?
		end
	else
		return nil, "NO POSTS FOR YEAR "..y --NO POSTS FOR THAT YEAR
	end
end

blogman.search = function(qstr)
	local posttab = {}
	-- go through table year by year
	for y, ytab in pairs(blogman.toc) do
		for m, mtab in pairs(ytab) do
			if config.dirres == "year" then
-- TODO: recursive
				if m:find(qstr) then
					table.insert(posttab,{y,m})
				end
			else
				for d, dtab in pairs(mtab) do
					if config.dirres == "month" then
						print(d)
						if d:find(qstr) then
							table.insert(posttab,{y,m,d})
						end
					elseif config.dirres == "day" then
						for pid, ptab in pairs(dtab) do
							if pid:find(qstr) then
								table.insert(posttab,{y,m,d,ptab})
							end
						end
					else
						return nil, "UNKNOWN dirres"
					end
				end
			end
		end
	end
	return posttab
end

blogman.mkurl = function(a,b,c,d)
	return table.concat({a,b,c,d},"/")
end

blogman.index = function(year, month, day)
	local restab = {}
	if not year then
		for y, ytab in pairs(blogman.toc) do
			-- what is our res?
			for m, mtab in pairs(ytab) do
				if config.dirres == "year" then
					-- Each post should have a .month and a .day
					table.insert(restab,table.concat({y,mtab.Month,mtab.Day,m},"/"))
				else
					for d, dtab in pairs(mtab) do
						if config.dirres == "month" then
							table.insert(restab,table.concat({y,m,dtab.Day or "00",d},"/"))
						else
							for pid, ptab in pairs(dtab) do
								table.insert(restab,table.concat({y,m,d,pid},"/"))
							end
						end
					end
				end
			end
		end
	elseif not month then
		local y = year
		for m, mtab in pairs(blogman.toc[year] or {}) do
			for d, dtab in pairs(mtab) do
				if config.dirres == "month" then
					table.insert(restab,table.concat({y,m,dtab.Day,d},"/"))
				else
					for pid, ptab in pairs(dtab) do
						table.insert(restab,table.concat({y,m,d,pid},"/"))
					end
				end
			end
		end
	elseif not day then
		local y = year
		local m = month
		for d, dtab in pairs(blogman.toc[year][month] or {}) do
			table.insert(restab,table.concat({y,m,dtab.Day,d},"/"))
		end
	else
		local y = year
		local m = month
		local d = day
		for pid, ptab in pairs(blogman.toc[year][month][day]) do
			table.insert(restab,table.concat({y,m,d,pid},"/"))
		end
	end
	return restab
end

blogman.loadtoc = function()
	blogman.toc = dofile(blogman.config.root.."/"..blogman.config.bloginfo)
end
blogman.savetoc = function()
	f,err = io.open(blogman.config.root.."/"..blogman.config.bloginfo,"w")
	if err then 
		return nil, err
	end
	f:write("return ")
	f:write(dumptable(blogman.toc))
	f:close()
	return true
end
