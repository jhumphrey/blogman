config = {
	root	= "blog", -- where do we store all the posts?
	bloginfo = "toc.lua", -- the Table of Contents for your blog
	dirres = "month" -- year | month | day -- split by year month or day?
}

return config
