-- check to see if we have a dumptable function
dumptable = dumptable or function(t)
    
    if type(t) == "string" then
        t = t:gsub("\"","\\\"")
        t = t:gsub("\n","\\n")
        return table.concat({"\"",t,"\""})
    elseif type(t) == "number" then
        return tostring(t)
    elseif type(t) == "function" then
        return dumptable(string.dump(t))
    elseif type(t) == "boolean" then   
        if t then  
            return "true"
        else
            return "false"
        end
    elseif type(t) == "table" then
        local pairstrings = {}
        for k, v in pairs(t) do
            table.insert(pairstrings,table.concat({"[",dumptable(k),"] = ",dumptable(v),","}))
        end
        return table.concat({"{",table.concat(pairstrings,"\n"),"}"})
    else
        return "nil --[[UNKNOWN TYPE "..type(t).."--]]" 
    end
end

blogman = blogman or {}
blogman.config = require "blogman.config"
require "blogman.post"
require "blogman.tocman"

blogman.loadtoc()

return blogman
