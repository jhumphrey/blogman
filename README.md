# *BLOGMAN*	A blog manager written in Lua.
[Terse Version](README)
# What is it?
Kinda like "лагман" but "благ" instead of "лаг"

If you didn't understand that, don't worry, I am not sure I did either.
So this is basically a blog framework to work with your lua program.
You could use it with a [web application](https://github.com/keplerproject), or a [gopher hole](https://gitlab.com/jhumphrey/krater),
or just directly as a personal journal. You could use it in many other ways too. If you come up with something let me know!

So let's talk about **how this works**
So it creates a directory for the blog. (if you haven't crated one already.)
And then it makes some sub-directories corresponding to year, month, and day depending on how you configure it.
And it saves all your posts in those directories. No, no, no, it isn't a file per post, that's messy!
Instead, it bundles posts together in larger files. and then acceses those. 
They are just lua tables, and the keys are Day, Month, Year, Time, Body, and maybe Title, but I don't have that implemented yet.
There is a lot I don't have implemented yet.

So here is where the blog manager comes in. It creates and retreives posts and automatically files them away in the right place.

## How do I install the crazy thing!
~~Installing is simple:
`sudo make install`~~
Oh, wait, there is no Makefile. No problem, you can install by hand!
No, no, no, now don't cry, it's OK. **I** will help you install this.
Go to your project directory.
you can just type the following
```sh
# download to the project directory
git clone https://gitlab.com/jhumphrey/blogman.git

# then edit the config file with the editor of your choice
ed blogman/config.lua

#to exit ed type q
```
And we are done! 
See! that wasn't so bad after all! If you thought that was bad, wait till you see my assembly code! 😝 XD

### Simple hack to work around the bug

So, lua appearantly doesn't accept init.lua 
you can currently workaround this with
```
cp blogman/init.lua blogman.lua
```
or,

```sh
echo require "blogman/init.lua" >blogman.lua
```

### But what if I want to install it system wide?
Okay, so instead of downloading to your project, download it to the lua libraries directory.
But if you want to use it in several places you need to do a bit of patching.
The following snippet assumes that you are in the blogman directory in your lua libraries collection.

I will assume that your blog directory is called `blog`

```sh
ed init.lua
29s/\"blogman/\"blog/
w
q
```

If you have multiple projects simply do `29d` instead of `29s`, and make certain to type:
`blogman.config = require "<path to config file>"`

Alternately, you could do:
```ed
29s/\=/\= blogman.config or/
```
and then just make sure to req' your config first.

## Ok that looks cool, but how do I use it?
First, you probably should configure it. Edit config.lua, It should be self explanatory, if you need help, file an issue.
Ok, maybe you need a little help. Here are what each of the options do
<table>
<tr>
<td>root</td><td>This is the name of the directory where blogman stores all the blog posts. I would advise making a dedicated directory.</td>
</tr>
<tr>
<td>bloginfo</td><td>this is the "table of contents" file. It is stored within the config.root directory</td>
</tr>
<tr>
<td>dirres</td><td>This is one that is kinda important. Do you blog like a thousand times every day? you probably want to set dirres to day
Do you blog a reasonable amount? month is for you.
Are you one of these people who is busy doing other things? You might get away with year. 
</td>
</tr>
</table>
**REMEMBER:** config.lua won't do a thing unless it is in the installation directory!

So, once you have it all configg'ed up and installed, you can require it into your project.
```lua
local blogman = require "blogman"
```

So now that you have it, you can do stuff with it.
So let me teach by example.
```lua
local blogman = require "blogman"

-- Let's create a post:
blogman.addpost("MyFirstPost",[[
This is my first post with blogman!
Whoohoo!!!!!! I just want to give 
the creator of Blogman gobs of money!
]])

-- Let's read a post:
-- posts are addressable in the form year-month-day-title or year/month/day/title
-- so we need to get the current date
local posttime = os.date("%Y %m %d")
local y,m,d = string.find(posttime,"(%d+) (%d+) (%d+)")
local mypost = blogman.getpost(y,m,d,"MyFirstPost")
print("MyFirstPost")
print("---")
print(mypost.body)
print("---")

-- OK, let's change the post
blogman.editpost(y,m,d,"MyFirstPost",[[This is my first post. I am NOT gonna give the creator gobs of money,
But I am going to buy him 500 pounds of grapefruit!
]])

-- I don't remember what the date was, let's just look for that title about
postarray = blogman.find("First")

-- now, let's link to that post.
print("The url is: "..blogman.mkurl(y,m,d,"MyFirstPost"))

-- that is a terrible post, I didn't mean it, delete it!
blogman.deletepost(y,m,d,pid)
```

That is the basics for how to use this framework. Simple, right?

## How can I help?

How kind of you to ask?
I have a laundry list of features that need adding or need to be not added.
But before we get to that, let me just mention that the quality of the current code could be improved. 
There is a lot of repetion and boilerplate.

Here is a list of features:

* Add support for size limits on tables (it could help save memmory)
* Add some sort of framework for resources? (Do we really need that?)
* Add some sort of comment management?
* ~~Create a gerneral intelligence AI that will give Me a lifetime supply of Grapefruit and or pomegranite.~~

If you have any needed features file an issue, I am not saying I will add them, but I am not saying I won't either.

Good luck! You might need it ;)
