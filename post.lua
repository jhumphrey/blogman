blogman = blogman or {toc={}}

blogman.filepath = function(y,m,d,pid) -- internal function
-- pid is not used, it is just there to make things easier later on
	if blogman.config.dirres == "year" then
		-- go by y and p or m if not p
			return table.concat({blogman.config.root,y..".lua"},"/")
	elseif blogman.config.dirres == "month" then
			return table.concat({blogman.config.root,y,m..".lua"},"/")
	elseif blogman.config.dirres == "day" then
			return table.concat({blogman.config.root,y,m,d..".lua"},"/")
	else
		return nil, "UNKNOWN dirres "..blogman.config.dirres
	end
end
blogman.getcluster = function(y,m,d,pid) -- internal function
	local path, err = blogman.filepath(y,m,d,pid)
	if err then
		return nil, err
	end
	local f, err = loadfile(path)
	if err then
		if err:find("No such file") then return {} end
		return nil, err
	end
	return f()
end
blogman.setcluster = function(y,m,d,pid, newchunk, oneshot) -- Internal function
	local function mkdir(d)
		return os.execute("mkdir "..d)
	end
-- warning this replaces the table, use editpost or addpost
	local path, err = blogman.filepath(y,m,d,pid)
	if err then
		return nil, err
	end
	-- open the file
	local f, err = io.open(path,"w")
	if err then
		if err:find("No such file") and not oneshot then
			mkdir(blogman.config.root.."/"..y)
			if config.dirres ~= "year" then
				mkdir(blogman.config.root.."/"..y.."/"..m)
				if config.dirres ~= "month" then
					mkdir(blogman.config.root.."/"..y.."/"..m.."/"..d)
				end
			end
			return blogman.setcluster(y,m,d,pid,newchunk,true)
		end
		return nil, err
	end
	f:write("return ")
	f:write(dumptable(newchunk))
	f:close()
	return true
end

local function safeguard(s)
	s:gsub("\"","\\\"")
	s:gsub("\n","\\n")
	return s
end

blogman.addpost = function(title,contents)
	-- get the date
	-- TODO: are you really that lazy?!!! just get the date once man.
	local dtab={
		year = os.date("%Y"),
		month = os.date("%m"),
		day = os.date("%d"),
		time = os.date("%X")
	}
	
	local cy, err = blogman.getcluster(dtab.year,dtab.month,dtab.day,title)
	if err then
		return nil, err
	end
	cy[title] = {
		Day	= dtab.day,
		Month	= dtab.month,
		Year	= dtab.year,
		Time	= dtab.time,
		Body	= contents
	}

	blogman.setcluster(dtab.year,dtab.month,dtab.day,pid,cy)
	-- update blogman.toc
	
	if not blogman.toc[tostring(dtab.year)] then
		blogman.toc[tostring(dtab.year)] = {}
	end
	if blogman.config.dirres == "year" then
		blogman.toc[tostring(dtab.year)][title] = {Day=dtab.day,Month=dtab.month,Time=dtab.time,Body=safeguard(contents:sub(1,100))}
	else
		if not blogman.toc[tostring(dtab.year)][tostring(dtab.month)] then
			blogman.toc[tostring(dtab.year)][tostring(dtab.month)] = {}
		end
		if blogman.config.dirres == "month" then
			blogman.toc[tostring(dtab.year)][tostring(dtab.month)][title] = {Day=dtab.day,Time=dtab.time,Body=safeguard(contents:sub(1,100))}
		elseif blogman.config.dirres == "day" then
			if not blogman.toc[tostring(dtab.year)][tostring(dtab.month)][tostring(dtab.day)] then
				blogman.toc[tostring(dtab.year)][tostring(dtab.month)][tostring(dtab.day)] = {}
			end
			blogman.toc[tostring(dtab.year)][tostring(dtab.month)][tostring(dtab.day)][title] = {Time=dtab.time,Body=safeguard(contents:sub(1,100))}
		end
		-- update TOC
	end
end

blogman.getpost = function(y,m,d,p)
	tabpath, err = blogman.filepath(y,m,d,pid)
	if err then
		return nil, err
	end
-- TODO: would it be better to have a universal pt?
	local pt,err = loadfile(tabpath)
	if err then
		return nil, err
	end
	return pt()[p]
end

blogman.editpost = function(y,m,d,pid,newcontent)
	newcontent 	= newcontent	or pid	 or d
	pid		= pid		or d	 or m
--	local tabpath, err = blogman.filepath(y,m,d,pid)
	local cc, err = blogman.getcluster(y,m,d,pid)
	if err then
		return nil, err
	end
	cc[pid] = newcontent
	blogman.setcluster(y,m,d,pid,newcontent)
end

blogman.deletepost = function(y,m,d,pid)
	local cc, err = blogman.getcluster(y,m,d,pid)
	if err then	
		return nil, err
	end
	cc[pid] = nil
	blogman.setcluster(y,m,d,pid,cc)
	-- update toc
	if not blogman.toc[y] then return end --We don't have to fix it
	if blogman.config.dirres == "year" then
		
		blogman.toc[tostring(y)][pid] = nil
	else
		if not blogman.toc[y][m] then return end
		if blogman.config.dirres == "month" then
			blogman.toc[y][m][pid] = nil
		elseif blogman.config.dirres == "day" then
			if not blogman.toc[y][m][d] then
				return
			end
			blogman.toc[y][m][d][pid] = nil
		end
	end
end
